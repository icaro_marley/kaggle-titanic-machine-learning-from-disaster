# -*- coding: utf-8 -*-

'''
train the model with all the training data
'''

import pandas as pd
import process_tools  
import parameters
from sklearn.externals import joblib

import importlib
importlib.reload(process_tools)
importlib.reload(parameters)

df_train = pd.read_csv(parameters.path_data_train)
df_train = process_tools.compute_priority(df_train)


# non priority
X_train = process_tools.process_nonpriority_X(df_train)
y_train = process_tools.process_nonpriority_y(df_train)

model = process_tools.create_model()

model.fit(X_train,y_train.values.ravel())

joblib.dump(model, parameters.path_model_0) 


#priority
X_train = process_tools.process_priority_X(df_train)
y_train = process_tools.process_priority_y(df_train)

model = process_tools.create_model()

model.fit(X_train,y_train.values.ravel())

joblib.dump(model, parameters.path_model_1) 
