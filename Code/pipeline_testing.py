# -*- coding: utf-8 -*-
'''
preprocessing & analysis, training and test
define process function which turns a dataset into a input for a model

'''
import importlib
import pandas as pd
from sklearn.model_selection import train_test_split
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import re
from parameters import path_data_train, random_state
import numpy as np 
import process_tools
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVC

importlib.reload(process_tools)

df = pd.read_csv(path_data_train)

target = 'Survived'

explanatory = np.delete(df.columns,\
                        np.argwhere(df.columns == target))


df_train, df_test = train_test_split(df, test_size=0.33, random_state=random_state)
df_train = process_tools.compute_priority(df_train)
df_test = process_tools.compute_priority(df_test)




importlib.reload(process_tools)
process_tools.cross_val(df,5)

#"""
# nulls
df_train.isnull().any()
# Age, Cabin, Embarked

'''
# fill age
#corr = X_train.corr()
# plot the heatmap
#sns.heatmap(corr)
# Pclass Parch


X_test_Age = X_test[~ X_test['Age'].isnull()][columns]
y_test_Age = X_test[~ X_test['Age'].isnull()][['Age']]
from sklearn.metrics import mean_absolute_error
print(mean_absolute_error(y_test_Age,model.predict(X_test_Age).astype(int)))
# 9.05658333333 error on testing data
'''


'''
#analysis
# nulls
df_train.isnull().any()
df_test.isnull().any()
# Cabin Embarked

# balance
df_train.groupby('ispriority')['Survived'].value_counts()
#sns.countplot(x='ispriority',hue='Survived', data=df_train) 
'''


"""
# not priority
X_train = df_train[df_train['ispriority'] == 0].copy()




# analysis 
'''
X_train[target].value_counts()
# unbalanced for Survival

X_train.describe()

X_train['Age'].plot(kind='hist')
X_train['Pclass'].value_counts().plot(kind='bar')

X_train.isnull().any()
X_train['Cabin'].value_counts(dropna=False) # NaN 307, won't use

sns.countplot(x='Pclass',hue='Survived', data=X_train) # might be good
sns.countplot(x='Embarked',hue='Survived', data=X_train) # might be good
sns.swarmplot(x="Fare", y=target, data=X_train,orient='h') # seem's ok
sns.swarmplot(x="Age", y=target, data=X_train,orient='h')  # trend for very old males dying
sns.swarmplot(x="Parch", y=target, data=X_train,orient='h') # more parch, less chance for survival
sns.swarmplot(x="SibSp", y=target, data=X_train,orient='h') # more parch, less chance for survival

sns.countplot(x='Ticket',hue='Survived', data=X_train) # not good

ticket_count = X_train['Ticket'].value_counts() 
ticket_count[ticket_count>1] # shared tickets
'''

# feature engineering

# shared tickets
ticket_count = X_train['Ticket'].value_counts() 
shared_ticket_list = ticket_count[ticket_count>1].index
X_train['Shared_ticket'] = X_train['Ticket'].apply(lambda x: 1 if x in shared_ticket_list else 0)

# cabin level 
level_0_list = ['t','r','s','n'] # boat deck cabins
def compute_level(value):
    level = value
    if not pd.isnull(value):
        level = re.sub('[0-9]+','',value)[0].lower()
        if level in level_0_list:
            level = 0
        else:
            level = ord(level[0].lower()) - 96
    return level
X_train['Level'] = X_train['Cabin'].apply(lambda x:compute_level(x))
level_allocations = X_train[['Pclass','Level']].dropna()
pclass_level_dict = {}
for n_class in level_allocations.Pclass.unique():
    pclass_level_dict.update({n_class:
        level_allocations[level_allocations['Pclass'] == n_class]\
        ['Level'].mode().astype(int).values[0]})           
def fill_level(row):
    level = row['Level']
    if np.isnan(level):
        level = pclass_level_dict[row['Pclass']]
    return level
X_train['Level'] = X_train.apply(lambda x:fill_level(x),axis=1)

# cabin known
X_train['Cabin_known'] = X_train['Cabin'].apply(lambda x: 0 if pd.isnull(x) else 1)

# fill cabin 
X_train['Cabin'] = X_train['Cabin'].fillna('unknown')

# name lenght
X_train['Name_len'] = X_train['Name'].apply(lambda x:len(x))

#family_size
X_train['Family_Size']=X_train['SibSp'] + X_train['Parch']

# title
def check_titles(row):
    title = re.findall(', ([A-Za-z]+)',row['Name'])[0]
    return title
X_train['Title']=X_train.apply(check_titles,axis=1)
title_dict= {
"Capt": "Officer",
"Col": "Officer",
"Major": "Officer",
"Jonkheer": "Sir",
"Don": "Sir",
} 
X_train['Title'] = X_train['Title'].replace(title_dict)
title_counts = X_train['Title'].value_counts()
frequent_titles = list(title_counts[title_counts > 10].index)
def process_title(value):
    if value not in frequent_titles:
        return 'other'
    return value
X_train['Title'] = X_train['Title'].apply(lambda x: process_title(x))

'''
# few known values
X_train['Cabin_known'].value_counts().plot(kind='bar')
# nulls filled
#X_train.isnull().any().any()


sns.swarmplot(x='Fare',y='Shared_ticket', data=X_train,orient='h') # higher size, less chance
# high fares, shared ticket

sns.countplot(x="Cabin_known", hue='Survived', data=X_train) # seem good
sns.countplot(x="Shared_ticket", hue='Survived', data=X_train) # doesn't seem good
sns.swarmplot(x='Name_len',y=target, data=X_train,orient='h') # might be good
sns.countplot(x="Level", hue='Survived', data=X_train) # seems good
sns.swarmplot(x='Family_Size',y=target, data=X_train,orient='h') # higher size, less chance
sns.countplot(x='Title',hue='Survived', data=X_train) # seems good
'''

# variable selection
X_train_ = X_train[[
    #'Age',  
    #'Fare',
    #'Cabin_known',
    #'Name_len',
    #'Level',
    #'Pclass',
    #'Embarked',
    #'Family_Size',
    #'Title',
    ]]
# dummification on Sex and Embarked
X_train_ = pd.get_dummies(X_train_,drop_first=True)

#train
y_train = df_train[df_train['ispriority'] == 0][target].copy()
model = process_tools.create_model()
model = RandomForestClassifier(n_estimators=100,random_state=random_state)
model.fit(X_train_,y_train.values.ravel())
# evaluating
ytrue = y_train
ypred = model.predict(X_train_)
print(accuracy_score(ytrue,ypred))
print(confusion_matrix(ytrue,ypred))
"""

"""
importlib.reload(process_tools)
X_train = process_tools.process_nonpriority_X(df_train)
y_train = process_tools.process_nonpriority_y(df_train)
X_test = process_tools.process_nonpriority_X(df_test,train=False)
y_test = process_tools.process_nonpriority_y(df_test)
# training
model = process_tools.create_model()
model.fit(X_train,y_train.values.ravel())

# evaluating
print('train')
ytrue = y_train
ypred = model.predict(X_train)
print(accuracy_score(ytrue,ypred))
print(confusion_matrix(ytrue,ypred))
print('test')
ytrue = y_test
ypred = model.predict(X_test)
print(accuracy_score(ytrue,ypred))
print(confusion_matrix(ytrue,ypred))


result_df = pd.DataFrame()
result_df['feature'] = X_train.columns
result_df['coef'] = model.coef_[0]
result_df.set_index('feature').sort_values('coef',ascending=False).plot(kind='bar'
"""









#"""
# priority
X_train = df_train[df_train['ispriority'] == 1].copy()

# analysis 
'''

X_train[target].value_counts()
# unbalanced for Survival

X_train.describe()

X_train['Age'].plot(kind='hist')
X_train['Pclass'].value_counts().plot(kind='bar')

X_train.isnull().any() # cabin and Embarked
X_train['Embarked'].value_counts(dropna=False) # NaN 1
X_train['Cabin'].value_counts(dropna=False) # NaN  149, fill with unknown

sns.countplot(x='Pclass',hue='Survived', data=X_train) # very good
sns.countplot(x='Embarked',hue='Survived', data=X_train) # good
sns.swarmplot(x="Fare", y=target, data=X_train,orient='h') # seem's good
sns.swarmplot(x="Age", y=target, data=X_train,orient='h')  # seem's good
sns.swarmplot(x="Parch", y=target, data=X_train,orient='h') # seem's ok
sns.swarmplot(x="SibSp", y=target, data=X_train,orient='h') # doesn't seem good
'''




# feature engineering


# shared tickets
ticket_count = X_train['Ticket'].value_counts() 
shared_ticket_list = ticket_count[ticket_count>1].index
X_train['Shared_ticket'] = X_train['Ticket'].apply(lambda x: 1 if x in shared_ticket_list else 0)

# cabin level 
level_0_list = ['t','r','s','n'] # boat deck cabins
def compute_level(value):
    level = value
    if not pd.isnull(value):
        level = re.sub('[0-9]+','',value)[0].lower()
        if level in level_0_list:
            level = 0
        else:
            level = ord(level[0].lower()) - 96
    return level
X_train['Level'] = X_train['Cabin'].apply(lambda x:compute_level(x))
level_allocations = X_train[['Pclass','Level']].dropna()
pclass_level_dict = {}
for n_class in level_allocations.Pclass.unique():
    pclass_level_dict.update({n_class:
        level_allocations[level_allocations['Pclass'] == n_class]\
        ['Level'].mode().astype(int).values[0]})           
def fill_level(row):
    level = row['Level']
    if np.isnan(level):
        level = pclass_level_dict[row['Pclass']]
    return level
X_train['Level'] = X_train.apply(lambda x:fill_level(x),axis=1)

# cabin known
X_train['Cabin_known'] = X_train['Cabin'].apply(lambda x: 0 if pd.isnull(x) else 1)

# fill cabin 
X_train['Cabin'] = X_train['Cabin'].fillna('unknown')

# name lenght
X_train['Name_len'] = X_train['Name'].apply(lambda x:len(x))

#family_size
X_train['Family_Size']=X_train['SibSp'] + X_train['Parch']


# fill embarked
X_train['Embarked'] = X_train['Embarked'].fillna(X_train['Embarked'].mode().values[0])

# title
def check_titles(row):
    title = re.findall(', ([A-Za-z]+)',row['Name'])[0]
    return title
X_train['Title']=X_train.apply(check_titles,axis=1)
title_dict= {
"the": "Lady",
"Dona": "Lady",
"Mme": "Mrs",
"Mlle": "Miss",
"Ms": "Mrs",
} 
X_train['Title'] = X_train['Title'].replace(title_dict)
title_counts = X_train['Title'].value_counts()
frequent_titles = list(title_counts[title_counts > 10].index)
def process_title(value):
    if value not in frequent_titles:
        return 'other'
    return value
X_train['Title'] = X_train['Title'].apply(lambda x: process_title(x))

'''
# nulls filled
X_train.isnull().any().any()


#analysis
sns.countplot(x="Cabin_known", hue='Survived', data=X_train) # seem good
sns.countplot(x="Shared_ticket", hue='Survived', data=X_train) # seem's good
sns.swarmplot(x='Name_len',y=target, data=X_train,orient='h') # might be good
sns.countplot(x="Level", hue='Survived', data=X_train) # seems good
sns.swarmplot(x='Family_Size',y=target, data=X_train,orient='h') # seem's ok
sns.countplot(x='Title',hue='Survived', data=X_train) # seems good


'''


# variable selection
X_train = X_train[[
    'Pclass',
    'Embarked',
    'Fare',
    'Age', 
    'Cabin_known',
    'Shared_ticket',
    'Name_len',
    'Level',
    'Family_Size',
    'Title',
    ]]


# dummification on Sex and Embarked
X_train = pd.get_dummies(X_train,drop_first=True)
#"""


"""
importlib.reload(process_tools)
X_train = process_tools.process_priority_X(df_train)
y_train = process_tools.process_priority_y(df_train)
X_test = process_tools.process_priority_X(df_test,train=False)
y_test = process_tools.process_priority_y(df_test)

# X_train, y_train
# training
model = process_tools.create_model()
model.fit(X_train,y_train.values.ravel())

# evaluating
print('train')
ytrue = y_train
ypred = model.predict(X_train)
print(accuracy_score(ytrue,ypred))
print(confusion_matrix(ytrue,ypred))
print('test')
ytrue = y_test
ypred = model.predict(X_test)
print(accuracy_score(ytrue,ypred))
print(confusion_matrix(ytrue,ypred))
#"""