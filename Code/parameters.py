# -*- coding: utf-8 -*-

'''
parameters shared for all scripts

'''


path_data = '../Modified Data/'
path_result = '../Results/'

path_data_train = path_data + 'train.csv'
path_data_test = path_data + 'test.csv'
path_output = path_result + 'output.csv'
path_model_0 = path_result + 'model_0.pkl'
path_model_1 = path_result + 'model_1.pkl'

random_state = 100