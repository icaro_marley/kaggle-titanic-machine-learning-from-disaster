# -*- coding: utf-8 -*-
'''

process function
create model function


'''

import parameters
from parameters import random_state, path_output
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import re
from sklearn.model_selection import KFold
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
import pickle


def cross_val(df_input,n):
    score_list = []
    kf = KFold(n_splits=n,random_state=random_state)
    for train_index, test_index in kf.split(df_input):
        df_train = df_input.loc[train_index]
        df_test = df_input.loc[test_index]
        df_train = compute_priority(df_train)
        df_test = compute_priority(df_test)
        
        # non priority
        X_train = process_nonpriority_X(df_train)
        y_train = process_nonpriority_y(df_train)
        X_test = process_nonpriority_X(df_test,train=False)
        y_test = process_nonpriority_y(df_test)
        model = create_model()
        model.fit(X_train,y_train.values.ravel())
        
        ytrue = y_test
        ypred = model.predict(X_test)
        score_aux_0 = accuracy_score(ytrue,ypred)
        
        #priority
        X_train = process_priority_X(df_train)
        y_train = process_priority_y(df_train)
        X_test = process_priority_X(df_test,train=False)
        y_test = process_priority_y(df_test)
        model = create_model()
        model.fit(X_train,y_train.values.ravel())         
        
        ytrue = y_test
        ypred = model.predict(X_test)
        score_aux_1 = accuracy_score(ytrue,ypred)
        # score
        score_list.append((score_aux_0 + score_aux_1)/2)

    score = np.mean(score_list)
    return score


def create_model(class_weight = None):
    model = RandomForestClassifier(random_state=random_state,n_estimators=100,max_depth=5,min_samples_leaf=5)
    #model = SVC(kernel='linear',random_state=random_state)
    #model = LogisticRegression(random_state=random_state,penalty='l2')
    return model


# save and load important feature engineering values
def save_priority_train_values(dict_values):
    with open('train_values_priority_aux.pkl', 'wb') as file:
        pickle.dump(dict_values, file, pickle.HIGHEST_PROTOCOL)
    
def load_priority_train_values():
    with open('train_values_priority_aux.pkl','rb') as file:    
        dict_values = pickle.load(file)
    return dict_values

def save_nonpriority_train_values(dict_values):
    with open('train_values_nonpriority_aux.pkl', 'wb') as file:
        pickle.dump(dict_values, file, pickle.HIGHEST_PROTOCOL)
    
def load_nonpriority_train_values():
    with open('train_values_nonpriority_aux.pkl','rb') as file:    
        dict_values = pickle.load(file)
    return dict_values

def process_nonpriority_X(df_input, train=True):
    x = df_input
    x = x[x['ispriority'] == 0].copy()
    
    if not train:
        dict_values = load_nonpriority_train_values()
    else:
        dict_values = {}

    # feature engineering 

    # cabin level 
    # shared tickets
    if train:
        ticket_count = x['Ticket'].value_counts() 
        shared_ticket_list = ticket_count[ticket_count>1].index
        dict_values['shared_ticket_list'] = shared_ticket_list
    else:
        shared_ticket_list = dict_values['shared_ticket_list']
    x['Shared_ticket'] = x['Ticket'].apply(lambda x: 1 if x in shared_ticket_list else 0)

    # cabin level 
    level_0_list = ['t','r','s','n'] # boat deck cabins
    def compute_level(value):
        level = value
        if not pd.isnull(value):
            level = re.sub('[0-9]+','',value)[0].lower()
            if level in level_0_list:
                level = 0
            else:
                level = ord(level[0].lower()) - 96
        return level
    x['Level'] = x['Cabin'].apply(lambda x:compute_level(x))
    
    if train:
        level_allocations = x[['Pclass','Level']].dropna()
        pclass_level_dict = {}
        for n_class in level_allocations.Pclass.unique():
            pclass_level_dict.update({n_class:
                level_allocations[level_allocations['Pclass'] == n_class]\
                ['Level'].mode().astype(int).values[0]})  
        dict_values['pclass_level_dict'] = pclass_level_dict
    else:
        pclass_level_dict = dict_values['pclass_level_dict']
         
    def fill_level(row):
        level = row['Level']
        if np.isnan(level):
            level = pclass_level_dict[row['Pclass']]
        return level
    x['Level'] = x.apply(lambda x:fill_level(x),axis=1)
    
    # cabin known
    x['Cabin_known'] = x['Cabin'].apply(lambda x: 0 if pd.isnull(x) else 1)
    
    # fill cabin 
    x['Cabin'] = x['Cabin'].fillna('unknown')
    
    # name lenght
    x['Name_len'] = x['Name'].apply(lambda x:len(x))
    
    #family_size
    x['Family_Size']=x['SibSp'] + x['Parch']
    
    # title
    def check_titles(row):
        title = re.findall(', ([A-Za-z]+)',row['Name'])[0]
        return title
    x['Title']=x.apply(check_titles,axis=1)
    title_dict= {
    "Capt": "Officer",
    "Col": "Officer",
    "Major": "Officer",
    "Jonkheer": "Sir",
    "Don": "Sir",
    } 
    x['Title'] = x['Title'].replace(title_dict)
    
    if train:
        title_counts = x['Title'].value_counts()
        frequent_titles = list(title_counts[title_counts > 10].index)
        dict_values['frequent_titles'] = frequent_titles
    else:
        frequent_titles = dict_values['frequent_titles']
    def process_title(value):
        if value not in frequent_titles:
            return 'other'
        return value
    x['Title'] = x['Title'].apply(lambda x: process_title(x))
    
    # fill Embarked
    if train:
        
        embarked_mode = x['Embarked'].mode().values[0]
        dict_values['embarked_mode'] = embarked_mode
    else:
        embarked_mode = dict_values['embarked_mode']
        
    x['Embarked'] = x['Embarked'].fillna(embarked_mode)   
        
    # variable selection
    x = x[[
        #'Age',  
        #'Fare',
        #'Cabin_known',
        #'Name_len',
        #'Level',
        #'Pclass',
        'Embarked',#
        'Family_Size',#
        'Title',#
        ]]
    
    # dummification on Sex and Embarked
    x = pd.get_dummies(x,drop_first=True)

    if train: # save train values
        save_nonpriority_train_values(dict_values)
    return x


def process_nonpriority_y(df_input):
    x = df_input
    x = x[x['ispriority'] == 0].copy()
    target = "Survived"
    return x[[target]]


def compute_priority(df_input):
    x = df_input
    x = x.copy()

    reg_file = 'age_regressor_aux.pkl'
    
    try:
        reg = joblib.load(reg_file)
    except:
        reg = None
    
    columns = ['Pclass','Parch'] # selected using forward search
    if reg is None:
        reg = RandomForestRegressor(n_estimators=150,random_state=random_state)
        x_Age = x[~ x['Age'].isnull()][columns]
        y_train_Age = x[~ x['Age'].isnull()][['Age']]
        reg.fit(x_Age,y_train_Age.values.ravel())
        joblib.dump(reg, reg_file) 
    
    def fill_Age(row):
        if np.isnan(row['Age']):
            vector = row[columns].values.reshape(1,-1)
            return reg.predict(vector).astype(int)[0]
        return row['Age']
    
    x['Age'] = x.apply(lambda x: fill_Age(x),axis=1)
    
    # ispriority
    def check_priority(row):
        if  row['Sex'] == 'female':
            return 1
        return 0
    x['ispriority'] = x.apply(lambda x: check_priority(x),axis=1)
    
    return x



def process_priority_X(df_input, train = True):
    x = df_input
    x = x[x['ispriority'] == 1].copy()

    if not train:
        dict_values = load_priority_train_values()
    else:
        dict_values = {}
        
    # feature engineering 
    
    # shared tickets
    if train:
        ticket_count = x['Ticket'].value_counts() 
        shared_ticket_list = ticket_count[ticket_count>1].index
        dict_values['shared_ticket_list'] = shared_ticket_list
    else:
        shared_ticket_list = dict_values['shared_ticket_list']
    x['Shared_ticket'] = x['Ticket'].apply(lambda x: 1 if x in shared_ticket_list else 0)

    # cabin level 
    level_0_list = ['t','r','s','n'] # boat deck cabins
    def compute_level(value):
        level = value
        if not pd.isnull(value):
            level = re.sub('[0-9]+','',value)[0].lower()
            if level in level_0_list:
                level = 0
            else:
                level = ord(level[0].lower()) - 96
        return level
    x['Level'] = x['Cabin'].apply(lambda x:compute_level(x))
    
    if train:
        level_allocations = x[['Pclass','Level']].dropna()
        pclass_level_dict = {}
        for n_class in level_allocations.Pclass.unique():
            pclass_level_dict.update({n_class:
                level_allocations[level_allocations['Pclass'] == n_class]\
                ['Level'].mode().astype(int).values[0]})  
        dict_values['pclass_level_dict'] = pclass_level_dict
    else:
        pclass_level_dict = dict_values['pclass_level_dict']
         
    def fill_level(row):
        level = row['Level']
        if np.isnan(level):
            level = pclass_level_dict[row['Pclass']]
        return level
    x['Level'] = x.apply(lambda x:fill_level(x),axis=1)
    
    # cabin known
    x['Cabin_known'] = x['Cabin'].apply(lambda x: 0 if pd.isnull(x) else 1)
    
    # fill cabin 
    x['Cabin'] = x['Cabin'].fillna('unknown')
    
    # name lenght
    x['Name_len'] = x['Name'].apply(lambda x:len(x))
    
    #family_size
    x['Family_Size']=x['SibSp'] + x['Parch']
    
    # title
    def check_titles(row):
        title = re.findall(', ([A-Za-z]+)',row['Name'])[0]
        return title
    x['Title']=x.apply(check_titles,axis=1)
    title_dict= {
        "the": "Lady",
        "Dona": "Lady",
        "Mme": "Mrs",
        "Mlle": "Miss",
        "Ms": "Mrs",
    } 
    x['Title'] = x['Title'].replace(title_dict)
    
    if train:
        title_counts = x['Title'].value_counts()
        frequent_titles = list(title_counts[title_counts > 10].index)
        dict_values['frequent_titles'] = frequent_titles
    else:
        frequent_titles = dict_values['frequent_titles']
    def process_title(value):
        if value not in frequent_titles:
            return 'other'
        return value
    x['Title'] = x['Title'].apply(lambda x: process_title(x))
    
    # fill Embarked
    if train:
        embarked_mode = x['Embarked'].mode().values[0]
        dict_values['embarked_mode'] = embarked_mode
    else:
        embarked_mode = dict_values['embarked_mode']
        
    x['Embarked'] = x['Embarked'].fillna(embarked_mode)   
        
    # variable selection
    x = x[[
        'Pclass',#
        'Embarked',#
        'Fare',#
        'Family_Size',#
        #'Age', 
        #'Cabin_known',
        #'Shared_ticket',
        #'Name_len',
        #'Level',
        #'Title',
    ]]
        
    # dummification on Sex and Embarked
    x = pd.get_dummies(x,drop_first=True)

    if train: # save train values
        save_priority_train_values(dict_values)    

    return x


def process_priority_y(df_input):
    x = df_input
    x = x[x['ispriority'] == 1].copy()
    

    target = "Survived"
    return x[[target]]


