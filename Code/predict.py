# -*- coding: utf-8 -*-
'''
predict values and output them as csv

'''

import process_tools  
import pandas as pd
import parameters  
from sklearn.externals import joblib

import importlib
importlib.reload(process_tools)



output = pd.DataFrame()
df_test= pd.read_csv(parameters.path_data_test)
output['PassengerId'] = df_test['PassengerId']

df_test = process_tools.compute_priority(df_test)


# non priority
model = joblib.load(parameters.path_model_0) 
X_test_0 = process_tools.process_nonpriority_X(df_test,train=False)
X_test_0['Survived'] = model.predict(X_test_0)



# priority
model = joblib.load(parameters.path_model_1) 

X_test_1 = process_tools.process_priority_X(df_test,train=False)
X_test_1['Survived'] = model.predict(X_test_1)



results = X_test_0[['Survived']].append(X_test_1[['Survived']]).sort_index()





# join
output['Survived'] = results
output.to_csv(parameters.path_output,index=False)