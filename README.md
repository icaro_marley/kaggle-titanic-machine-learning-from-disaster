# Kaggle: Titanic: Machine Learning from Disaster 

Repositório para a competição do Kaggle "Titanic: Machine Learning from Disaster", onde dados sobre tripulantes do Titanic são analisados para entender fatores que influenciaram na morte ou na sobrevivência de cada um.
A melhor pontuação testada contra dados usados para o ranking foi de 82% de Acurácia.

URL: https://www.kaggle.com/c/titanic

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Sklearn (Machine Learning)